<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\UseUuid;

class Member extends Model
{
    use UseUuid;
    protected $fillable = [
        'name',
        'address',
        'photo'
    ];
}
