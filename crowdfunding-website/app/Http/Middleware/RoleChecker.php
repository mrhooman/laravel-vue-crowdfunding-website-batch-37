<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();

        if($user->role_id === "258d76c9-20c5-4f8a-afce-05b1f7209ba8") {
            return $next($request);
        }

        return response()->json([
            "message" => "Maaf Anda tidak memiliki hak akses untuk halaman ini"
        ]);
    }
}
