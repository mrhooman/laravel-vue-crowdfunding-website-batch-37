<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $user = auth()->user();

        if($user === null){
            return [
                'Status' => '01',
                'Message' => 'User UnAuthorized',
            ];
        }
        $data = response()->json($user);

        return [
            'Status' => '00',
            'Message' => 'User ditampikan!!',
            'Data' => $data->original
        ];
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return [
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt(request('password'))
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if(auth()->user() === null){
            return [
                'Status' => '01',
                'Message' => 'User UnAuthorized',
            ];
        }

        $users = auth()->user()->id;
        $user = User::where('id',$users)->first();

        $request->validate([
            'name' => 'required',
        ]);

        $path = $request->file('photo_profile')->store('public/photo-profile');
        

        $users = $user->update([
            'name' => $request['name'],
            'photo_profile' => $path,
        ]);
        
        $users = auth()->user()->id;
        $user = User::where('id',$users)->first();

        $data['user'] = $user;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User Berhasil Diedit',
            'data' => $data
        ]);
    }

    public function updatePassword(Request $request){

        if(auth()->user() === null){
            return [
                'Status' => '01',
                'Message' => 'User UnAuthorized',
            ];
        }

        $users = auth()->user()->id;
        $user = User::where('id',$users)->first();

        $request->validate([
            'email' => 'required|min:6',
            'password' => 'required|min:6|confirmed',
        ]);
        

        $users = $user->update([
            'email' => $request['email'],
            'password' => bcrypt(request('password')),
        ]);

        return [
            'messege' => 'Password Berhasil Diubah',
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function upload(Request $request) {
  
        
      
        // then you can save $imageName to the database
      
      }

    
}
