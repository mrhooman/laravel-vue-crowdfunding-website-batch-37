<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UseUuid;

class Otp_code extends Model
{
    use UseUuid;

    protected $fillable = ["otp","user_id","valid_until"];

    public function user(){
        return $this->belongsTo("App\Models\User", "user_id");
    }
}
