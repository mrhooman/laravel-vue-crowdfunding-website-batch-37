<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UseUuid;



class Role extends Model
{
    use UseUuid;
    protected $fillable = ['name'];

    public function users(){
        return $this->hasMany('App\User', 'role_id');
    }
}
