<?php

namespace App\Http\Controllers\Auth;

use App\Events\RegenerateOtpEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        $user = User::where('email', $request->email)->first();

        if($user === null){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email Tidak Terdaftar'
            ],404);
        }

        $user->generate_otp_code();

        $data['user'] = $user;
        
        event(new RegenerateOtpEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'OTP Code Berhasil digenerate',
            'data' => $data
        ],200);
    }
}
