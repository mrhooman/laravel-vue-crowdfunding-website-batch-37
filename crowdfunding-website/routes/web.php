<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/send-email', function () {
    return view('send_email');
});

Route::get('/send-email-regenerated-otp', function () {
    return view('send_email_regenerated');
});

Route::get('/', function(){
    return view('app');
});
// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::get('/route-1', function(){
//     return "masuk ke route-1, email terverifikasi";
// })->middleware('verifikasiEmail');

// Route::function(){
//     return "Selamat Datang Admin!!!";
// })->middleware(['verifikasiEmail','cekrole']);