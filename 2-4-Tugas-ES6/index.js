//Soal 1
const luasPersegiPanjang = (panjang, lebar) =>{
    return 'Luas Persegi Panjang : ' + panjang * lebar;
}

let kelilingPersegiPanjang = (panjang, lebar) =>{
    return 'Keliling Persegi Panjang : ' + 2 * (panjang + lebar);
}
console.log(`Soal 1:\n${luasPersegiPanjang(10,2)}`);
console.log(kelilingPersegiPanjang(10,2) + '\n');

//Soal 2
const literalFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName : () =>{
        console.log(`${firstName} ${lastName}`);
      }
    }
  }

console.log('Soal 2:')
literalFunction('Muhammad', 'Rijal').fullName()

//Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName,lastName,address,hobby} = newObject;

console.log('\nSoal 3:')
console.log(firstName,lastName,address,hobby);

//Soal 4 
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combinedArray = [...west, ...east]
console.log('\nSoal 4 : ')
console.log(combinedArray)

//Soal 5
const planet = "earth" 
const things = "human" 
var before = 'Lorem ' + things + ' dolor sit amet, ' + 'consectetur adipiscing elit,' + planet;
var after = `this planed called ${planet}, and ${things} live here`;

console.log(`\nSoal 5 : \nBefore : ${before} \nAfter : ${after}`)