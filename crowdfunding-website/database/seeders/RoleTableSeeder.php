<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Role::create([
        //     'name' => 'admin'
        // ]);
        
        // Role::create([
        //     'name' => 'user'
        // ]);

        Role::create([
            'name' => 'admin',
            'id' => '258d76c9-20c5-4f8a-afce-05b1f7209ba8'
        ]);

        Role::create([
            'name' => 'user',
            'id' => '64fdef11-af19-4e5c-80f5-a4f462a7ae94'
        ]);
    }
}

