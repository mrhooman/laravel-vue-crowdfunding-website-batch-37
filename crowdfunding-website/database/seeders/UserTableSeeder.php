<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    public function run(){
        // User::create([
        //     'name' => Str::random(10),
        //     'email' => Str::random(10).'@gmail.com',
        //     'password' => Hash::make('password'),
        //     'role_id' => '258d76c9-20c5-4f8a-afce-05b1f7209ba8',
        // ]);

        // User::create([
        //     'name' => Str::random(10),
        //     'email' => Str::random(10).'@gmail.com',
        //     'password' => Hash::make('password'),
        //     'role_id' => '64fdef11-af19-4e5c-80f5-a4f462a7ae94',
        // ]);
        // User::create([
        //     'name' => Str::random(10),
        //     'email' => Str::random(10).'@gmail.com',
        //     'password' => Hash::make('password'),
        //     'role_id' => '64fdef11-af19-4e5c-80f5-a4f462a7ae94',
        // ]);

        User::create([
             'name' => 'User Unverified',
             'email' => 'user1@gmail.com',
             'password' => Hash::make('password'),
             'role_id' => '64fdef11-af19-4e5c-80f5-a4f462a7ae94',
        ]);

        User::create([
            'name' => 'Admin Unverified',
            'email' => 'admin1@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => '258d76c9-20c5-4f8a-afce-05b1f7209ba8',
        ]);

        User::create([
            'name' => 'User Verified',
            'email' => 'userverif@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => '64fdef11-af19-4e5c-80f5-a4f462a7ae94',
            'email_verified_at' => '2022-08-10 05:47:38'
       ]);

       User::create([
           'name' => 'Admin Verified',
           'email' => 'adminverif@gmail.com',
           'password' => Hash::make('password'),
           'role_id' => '258d76c9-20c5-4f8a-afce-05b1f7209ba8',
           'email_verified_at' => '2022-08-10 05:47:38'
       ]);

       User::create([
        'name' => 'userdumb',
        'email' => 'userdum@gmail.com',
        'role_id' => '64fdef11-af19-4e5c-80f5-a4f462a7ae94',
    ]);
    }
    
}
