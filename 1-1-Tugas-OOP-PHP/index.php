<?php
trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian}";
    }

}

abstract class Fight { 
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function serang($lawan){
        echo "{$this->nama} sedang menyerang {$lawan->nama}, ";

        $lawan->diserang($this);
    }

    public function diserang($lawan){
        echo "{$this->nama} sedang diserang {$lawan->nama}";
        $this->darah = $this->darah - $lawan->attackPower / $this->defencePower;
    }

    protected function getInfo(){
        echo "<br>";
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Darah : {$this->darah}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Defence Power : {$this->defencePower}";
        echo "<br>";
        $this->atraksi();
    }

    abstract public function getInfoHewan();
}

class Elang extends Fight{
    public function __construct($string){
        $this->nama = $string;
        $this->jumlahKaki = 2;
        $this->keahlian = "Terbang Tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Elang";
        $this->getInfo();
    }
}

class Harimau extends Fight{
    public function __construct($string){
        $this->nama = $string;
        $this->jumlahKaki = 4;
        $this->keahlian = "Lari Cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Elang";
        $this->getInfo();
    }
}

class spasi{
    public static function tampilkan(){
        echo "<br> <br>";
    }
}


$elang1 = new Elang("elang_1");
$elang1->getInfoHewan();

spasi::tampilkan();

$harimau1 = new Harimau("harimau_1");
$harimau1->getInfoHewan();

spasi::tampilkan();

$elang1->serang($harimau1);

spasi::tampilkan();

$harimau1->getInfoHewan();

spasi::tampilkan();

$harimau1->serang($elang1);

spasi::tampilkan();

$elang1->getInfoHewan();


?>