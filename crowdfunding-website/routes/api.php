<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'prefix' => 'auth',
    'namespace' => 'App\Http\Controllers\Auth' // <--- TADI SAYA CUMAN NULIS 'namespace' => 'Auth'
], function(){
    Route::post('register', 'RegisterController');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController');
    Route::post('verifikasi-user', 'VerificationUserController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::group([
    'prefix' => 'profile',
    'namespace' => 'App\Http\Controllers\Profile'
], function(){
    Route::patch('update', 'ProfileController@update');
    Route::patch('updatePassword', 'ProfileController@updatePassword');
    Route::get('show-profile', 'ProfileController@index');
});

Route::get('member', 'App\Http\Controllers\MemberController@index');
Route::post('member', 'App\Http\Controllers\MemberController@store');
Route::put('member/{id}', 'App\Http\Controllers\MemberController@update');
Route::get('member/{id}', 'App\Http\Controllers\MemberController@show');
Route::delete('member/{id}', 'App\Http\Controllers\MemberController@destroy');
Route::post('member/{id}/upload-photo', 'App\Http\Controllers\MemberController@upload');

// Route::post('blog', function(){
//     return 'awokdoawkokd';
// });
