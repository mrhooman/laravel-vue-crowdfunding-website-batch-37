<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Otp_code;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Carbon;


class VerificationUserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'otp' => 'required',
        ]);

        $otp_code = Otp_code::where('otp', $request->otp)->first();

        if(!$otp_code) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'OTP Code Tidak Ditemukan !!!'
            ],400);
        }

        $now = Carbon::now();

        if($now > $otp_code->valid_until){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'OTP Code Tidak Berlaku, Silahkan Generate Ulang !!!'
            ],400);
        }

        //Update User
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();

        // Hapus Otp Code setelah dipakai 
        $otp_code->delete();
        
        $data['user'] = $user;
        
        return response()->json([
            'response_code' => '00  ',
            'response_message' => 'Akun Berhasil DiVerifikasi',
            'data' => $data
        ],200);
    }
}
