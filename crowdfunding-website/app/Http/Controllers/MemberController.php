<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use DB;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::table('members')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Member::create([
            'name' => $request['name'],
            'address' => $request['address'],
        ]);

        return response()->json([
            'response_code' => '00',
            'message' => 'member berhasil dibuat',
            'data' => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request ,$id)
    {
        $members = $request->id;
        $member = Member::where('id',$members)->first();
        return response()->json([
            'response_code' => '00',
            'message' => 'member berhasil Ditampilkan',
            'data' => $members]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $members = $request->id;
        $member = Member::where('id',$members)->first();

        // $request->validate([
        //     'name' => 'required',
        // ]);

        // $path = $request->file('photo_profile')->store('public/photo-profile');
        
        $members = $member->update([
            'name' => $request['name'],
            'description' => $request['description'],
            // 'photo_profile' => $path,
        ]);

        return response()->json([
            'response_code' => '00',
            'message' => 'member berhasil Diedit',
            'data' => $members]);
    }
    public function upload(Request $request){
        $member = Member::where('id',$request->id)->first();

        $path = $request->file('photo')->store('public/photo-profile');
        $memberUpdated = $member->update([
            'photo' => $path,
        ]);
        

        return response()->json([
            'response_code' => '00',
            'message' => 'Photo Berhasil DiUpload',
            'data' => $memberUpdated]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::where('id',$id)->first();
        $member->delete();
        return response()->json([
            'response_code' => '00',
            'message' => 'member berhasil dihapus']);
    }
}
