<?php 

namespace App\Traits;

use illuminate\Support\str;

trait UseUuid{

    public function serKeyType(){
        return 'string';
    }

    public function getIncrementing(){
        return false;
    }

    protected static function bootUseUuid(){
        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}

?>