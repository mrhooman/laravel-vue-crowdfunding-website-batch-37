<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\UseUuid;
use App\Models\Role;
use App\Models\otp_code;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, UseUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'photo_profile'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->role_id = $model->get_role_user();
        });

        static::created(function($model){
            $model->generate_otp_code();
        });
    }

    public function get_role_user(){
        $role = Role::where('name', 'user')->first();
        return $role->id;
    }

    public function generate_otp_code(){
        do{
            $random = mt_rand(100000, 999999);
            $check = otp_code::where('otp', $random)->first();
        }while($check);

        $now = Carbon::now();

        $otp_code = otp_code::updateOrCreate(
            ['user_id' => $this->id],
            [
                'otp' =>$random,
                'valid_until' => $now->addMinutes(5),
            ]
        );
    }

    /**  
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function role(){
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

    public function otp_code(){
        return $this->hasOne('App\Models\Otp_Code', 'user_id');
    }
}
