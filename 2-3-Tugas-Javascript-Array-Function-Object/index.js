// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
console.log('Soal 1 :');
for(var i=0; i < daftarHewan.length; i++){
    console.log(daftarHewan[i]);
}

// Soal 2
function introduce(name,age,address,hobby){
    var intro = 'Nama saya '+[name]+', umur saya '+[age]+' tahun, alamat saya di '+[address]+', dan saya punya hobby yaitu '+[hobby]+'!';
    return console.log(intro);
}

console.log('\nSoal 2 :');
introduce('rijal',21,'Bandung','gaming');

//Soal 3
function hitung_huruf_vokal(kata){
    // $words = $kata.split('');
    var hurufVokal = 'aiueo';
    var jumlahVokal = 0;
    for(var i=0; i < kata.length; i++){
        if(hurufVokal.indexOf(kata.toLowerCase()[i]) !== -1){
            jumlahVokal += 1;
        }
    }

    return console.log(jumlahVokal);
    
}

console.log('\nSoal 3 :');
hitung_huruf_vokal('Muhammad Rijal');
hitung_huruf_vokal('Annisa Dinda Mariska');

// Soal 4
function hitung($angka){
    return console.log(($angka*2)-2);
}

console.log('\nSoal 4 :');
hitung(0);
hitung(1);
hitung(2);
hitung(3);
hitung(4);
hitung(5);